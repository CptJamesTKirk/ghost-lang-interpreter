using System;
using System.Collections.Generic;

namespace Ghost.Lexer {

    public enum TType {
        IDENT, NUMBER, SYMBOL, KEYWORD, STRING,

        LBRACE, RBRACE, LPAREN, RPAREN, EOF,

        LSQUARE, RSQUARE
    }

    public class Token {
        public TType Type;
        public string Lexeme;
        public int Line = 1;

        public Token(TType type, string lexeme) {
            Type = type;
            Lexeme = lexeme;
        }

        public Token(TType type, string lexeme, int line) {
            Type = type;
            Lexeme = lexeme;
            Line = line;
        }
    }

    public class Lexer {
        public string Source;

        public int Current = 0;

        public int Line = 1;

        public List<string> Keywords = new List<string>();

        private char Advance() {
            if (Current == Source.Length) {
                return '\0';
            }

            return Source[Current++];
        }

        private char Peek() {
            if (Current == Source.Length) {
                return '\0';
            }

            return Source[Current];
        }

        private bool Check(char c) {
            if (Peek() == c) {
                Advance();
                return true;
            }

            return false;
        }

        private bool AtEnd() {
            return Current == Source.Length;
        }

        public bool IsAlpha(char c) {
            return ((c >= 'a' && c <= 'z') ||
				(c >= 'A' && c <= 'Z') ||
				(c == '_'));
        }

        private bool IsDigit(char c) {
            return c >= '0' && c <= '9';
        }

        private bool IsAlphaNumeric(char c) {
            return IsAlpha(c) || IsDigit(c);
        }

        private bool IsSymbol(char c) {
            return 
            c == '!' || c == '<' || c == '>' ||
            c == '|' || c == '&' || c == '=' ||
            c == '+' || c == '-' || c == '*' ||
            c == '/';
        }

        public Token GetToken() {

            while (! AtEnd()) {
                char c = Peek();
                
                if (c == '\n') {
                    Line++;
                    Advance();
                    continue;
                }

                if (c == ' ')  {
                    Advance();
                    continue;
                };

                if (c == ',') {
                    Advance();
                    return new Token(TType.SYMBOL, ",", Line);
                }

                if (c == '"') {
                    Advance();
                    string str = "";

                    while (! Check('"')) {
                        str += Advance();
                    }

                    return new Token(TType.STRING, str, Line);
                }

                if (c == '(')  {
                    Advance();
                    return new Token(TType.LPAREN, "(", Line);
                }
                if (c == ')') {
                    Advance();
                    return new Token(TType.RPAREN, ")", Line);
                }

                if (c == '[')  {
                    Advance();
                    return new Token(TType.LSQUARE, "[", Line);
                }
                if (c == ']') {
                    Advance();
                    return new Token(TType.RSQUARE, "]", Line);
                }

                if (c == '{')  {
                    Advance();
                    return new Token(TType.LBRACE, "{", Line);
                }
                if (c == '}') {
                    Advance();
                    return new Token(TType.RBRACE, "}", Line);
                }

                if (IsSymbol(c)) {
                    Advance();

                    if (c == '|' && Check('|')) {
                        Advance();
                        return new Token(TType.SYMBOL, "||", Line);
                    } 
                    if (c == '=' && Check('=')) {
                        Advance();
                        return new Token(TType.SYMBOL, "==", Line);
                    }

                    return new Token(TType.SYMBOL, c.ToString(), Line);
                }

                if (IsDigit(c)) return Number();

                if (IsAlpha(c)) return Ident();
                Advance();
            }

            return new Token(TType.EOF, "", Line);
        }

        public List<Token> GetTokens() {
            List<Token> result = new List<Token>();

            while (true) {
                Token tok = GetToken();

                result.Add(tok);

                if (tok.Type == TType.EOF) break;
            }

            return result;
        }

        private Token Ident() {
            string ident = "";
            while (IsAlphaNumeric(Peek())) ident += Advance();
            
            if (Keywords.Contains(ident))
                return new Token(TType.KEYWORD, ident);

            return new Token(TType.IDENT, ident);
        }

        private Token Number() {
            string num = "";
            while (IsDigit(Peek()) || Peek() == '.') num += Advance();
            
            return new Token(TType.NUMBER, num);
        }

        public Lexer(string source) {
            Source = source;
            Keywords.Add("print");
            Keywords.Add("let");
            Keywords.Add("fn");
            Keywords.Add("if");
            Keywords.Add("return");
        }
    }
}