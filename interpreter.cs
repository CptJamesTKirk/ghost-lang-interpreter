using System;
using System.Reflection;
using System.Collections.Generic;
using Ghost.Lexer;
using Ghost.Parser;

// fn func(num) { if (num == 1) { return 1 } return 5 }

namespace Ghost {
    public class Context {
        public Context Parent;
        public bool ReturnableTo = false;
        public object ReturnValue;
        public bool Returned = false;

        public Dictionary<String, object> Locals = new Dictionary<String, object>();
        public Dictionary<String, Function> Functions = new Dictionary<String, Function>();

        public void Set(string name, object value) {
            if (! Locals.ContainsKey(name))
                Locals.Add(name, value);
            else
                Locals[name] = value;
        }

        public object Get(string name) {
            if (! Locals.ContainsKey(name)) {
                if (Parent != null) return Parent.Get(name);
                
                try {
                    return GetFunc(name);
                }
                catch (Exception) {}

                throw new Exception($"Undefined reference to variable {name}");
            }

            return Locals[name];
        }

        public void SetFunc(string name, Function value) {
            if (! Functions.ContainsKey(name))
                Functions.Add(name, value);
            else
                Functions[name] = value;
        }

        public Function GetFunc(string name) {
            if (! Functions.ContainsKey(name)) {
                if (Parent != null) return Parent.GetFunc(name);

                throw new Exception($"Undefined reference to function {name}");
            }

            return Functions[name];
        }

        public Context() {}

        public Context(Context parent) {
            Parent = parent;
        }
    }

    public class ArrayCollection {
        public List<object> Values = new List<object>();
    }

    public class Function {
        public Interpreter Interpreter;
        public List<string> Args;
        public List<Statement> Body;

        public delegate object NativeFunc(Interpreter intrp, List<Expression> args);

        public NativeFunc Native;
        public object Target;
        public MethodInfo CSNative;

        public object Call(List<Expression> args) {
            if (Native != null) {
                return Native(Interpreter, args);
            }

            if (CSNative != null) {
                List<object> evaled = new List<object>();

                foreach (Expression expr in args) {
                    evaled.Add(expr.Accept(Interpreter));
                }

                return CSNative.Invoke(Target, evaled.ToArray());
            }

            Interpreter.AddContext();
            Interpreter.Scope.ReturnableTo = true;
            
            for (int i = 0; i < Args.Count; i++) {
                string ident = Args[i];

                Interpreter.Scope.Set(ident, args[i].Accept(Interpreter));
            }

            foreach (Statement stmt in Body) {
                stmt.Accept(Interpreter);
                if (Interpreter.Scope.Returned) break;
            }

            object val = Interpreter.Scope.ReturnValue;

            Interpreter.PopContext();

            return val;
        }

        public Function() {}
        
        public Function(Interpreter intrp, NativeFunc func)
         {
            Interpreter = intrp;
            Native = func;
        }
    }

    public class Interpreter {

        public Context Scope;
        Stack<Context> Contexts = new Stack<Context>();

        public void AddContext() {
            Contexts.Push(new Context(Scope));
            Scope = Contexts.Peek();
        }

        public void PopContext() {
            if (Contexts.Count > 1) {
                Scope = Contexts.Pop();
            }
            
            Scope = Contexts.Peek();
        }

        public void Execute(Expression expr) {

        }

        public void Print(object obj) {
            Console.WriteLine(obj);
        }

        public void FuncDef(string name, List<string> args, List<Statement> body) {
            Function func = new Function();

            func.Interpreter = this;
            func.Args = args;
            func.Body = body;
            
            Scope.SetFunc(name, func);
        }

        public object LastRet;

        public object Call(string name, List<Expression> args) {

            if (! Scope.Functions.ContainsKey(name)) {
                try {
                    object val = GetVar(name);

                    if (val is Function) {
                        LastRet = (val as Function).Call(args);

                        return LastRet;
                    }
                }
                catch (Exception) {}
            }

            Function func = Scope.GetFunc(name);

            LastRet = func.Call(args);
            return LastRet;
        }

        public object LateCall(List<Expression> args) {
            Function func = (LastRet as Function);

            if (func == null)
                throw new Exception($"Unable to call value of type '{TypeOf(LastRet)}'");

            return func.Call(args);
        }

        public object Index(ArrayCollection ary, object val) {
            int index = (int) Math.Floor((double) val);

            if (index >= ary.Values.Count)
                throw new Exception("Index was out of bounds");
            
            return ary.Values[index];
        }

        public void Return(object val) {
            while (! Scope.ReturnableTo) {
                if (Contexts.Count == 1)
                    throw new Exception("No enclosing function of which to return from");
                
                PopContext();
            }

            Scope.ReturnValue = val;
            Scope.Returned = true;
        }

        public object Block(List<Statement> stmts) {
            AddContext();

            foreach (Statement stmt in stmts) {
                stmt.Accept(this);
                if (Scope.Returned) return null;
            }

            PopContext();

            return null;
        }

        public ArrayCollection Array(List<Expression> values) {
            ArrayCollection list = new ArrayCollection();

            foreach (Expression expr in values) {
                list.Values.Add(expr.Accept(this));
            }

            return list;
        }

        public void SetVar(string name, object val) {
            Scope.Set(name, val);
        }

        public object GetVar(string name) {
            return Scope.Get(name);
        }

        public bool Defined(string name) {
            try {
                Scope.Get(name);
                return true;
            } catch(Exception) {
                return false;
            }
        }

        private string TypeOf(object obj) {
            if (obj is bool) return "bool";
            if (obj is double) return "number";

            return "unknown";
        }

        public void If(Expression condition, Statement block) {
            object val = condition.Accept(this);

            if (TypeOf(val) != "bool")
                throw new Exception("Only boolean values may be used in an if statement");

            if ((val as bool?) == true) {
                block.Accept(this);
            }
        }

        public object Binary(object left, Token op, object right) {
            if (op.Lexeme == "=") {
                SetVar(left.ToString(), right);
                return right;
            }

            if (op.Lexeme == "+") {
                return ((double) left) + ((double) right);
            }

            if (op.Lexeme == "*") {
                return ((double) left) * ((double) right);
            }

            if (op.Lexeme == "/") {
                return ((double) left) / ((double) right);
            }

            if (op.Lexeme == "-") {
                return ((double) left) - ((double) right);
            }

            if (op.Lexeme == "==") {
                return (left.ToString() == right.ToString());
            }

            return null;
        }

        public Interpreter() {
            AddContext();
            PopContext();

            foreach (KeyValuePair<string, Function> func in Standard.Construct(this)) {
                Scope.SetFunc(func.Key, func.Value);
            }
        }
    }
}