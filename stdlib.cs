using System;
using System.Reflection;
using System.Collections.Generic;
using Ghost.Parser;

namespace Ghost {
    public static class Standard {

        private static object DNOpen(Interpreter intrp, List<Expression> args) {
            Function func = new Function();

            string classname = args[0].Accept(intrp) as string;
            string methodname = args[1].Accept(intrp) as string;

            if (args.Count < 3) {
                func.Target = Type.GetType(classname);
                func.CSNative = Type.GetType(classname).GetMethod(methodname);

                return func;
            }
            
            string type = args[2].Accept(intrp) as string;

            
            func.Target = Type.GetType(classname);
            func.CSNative = Type.GetType(classname).GetMethod(methodname, new Type[] { Type.GetType(type) });

            return func;
        }

        public static Dictionary<string, Function> Construct(Interpreter intrp) {
            Dictionary<string, Function> result = new Dictionary<string, Function>();

            result.Add("dnopen", new Function(intrp, DNOpen));

            return result;
        }
    }
}