This is an interpreter for the spec found at https://github.com/jamiebuilds/ghost-lang/ written in C#.

The interpreter currently supports basic variables, functions and can use lists but can only run from a specific file as of now.

print keyword is temporary!


The following has correct output: 
```js
fn fact(num) {
    if (num == 1) {
        return 1
    }

    return num * fact(num - 1)
}

let i = [1, [5, 2], 4, 3]

print "Factorial of 3: "
print fact(3)
print "------ 12345 ------"
print i[0]
print i[1][1]
print i[3]
print i[2]
print i[1][0]```