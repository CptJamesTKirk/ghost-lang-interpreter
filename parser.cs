using System;
using System.Collections.Generic;
using Ghost;
using Ghost.Lexer;

namespace Ghost.Parser {

    public class ParsingError : Exception {
        public Token Tok;

        public ParsingError(Token tok, string msg) : base(msg) {
            Tok = tok;
        }
    }

    public class Statement {
        public Expression Expr;

        public string Name;
        public Expression Value;
        public bool NoDef = false;

        public bool End = false;

        public bool If = false;
        public Expression Condition;
        public Statement IfBlock;
        public bool FuncDef = false;
        public List<string> Arguments;
        public List<Statement> Block;
        public Expression Return;

        public object Accept(Interpreter intrp) {
            if (Name != null) {

                if (FuncDef) {
                    intrp.FuncDef(Name, Arguments, Block);
                    return null;
                }

                if (NoDef) {
                    if (! intrp.Defined(Name))
                        throw new Exception($"Undefined reference to variable '{Name}'");
                }
                
                if (NoDef && ! intrp.Defined(Name))
                    throw new Exception($"Undefined reference to variable '{Name}'");

                if (intrp.Defined(Name) && ! NoDef)
                        throw new Exception($"Cannot redeclare variable '{Name}'");

                intrp.SetVar(Name, Value.Accept(intrp));
            }

            if (If) {
                intrp.If(Condition, IfBlock);
                return null;
            }

            if (Return != null) {
                intrp.Return(Return.Accept(intrp));
                return null;
            }

            if (Block != null) {
                return intrp.Block(Block);
            }

            if (Expr != null) {
                return Expr.Accept(intrp);
            }

            return null;
        }

        public Statement() {}

        public Statement(Expression expr) {
            Expr = expr;
        }

        public Statement(List<Statement> block) {
            Block = block;
        }

        public Statement(string name, Expression value, bool nodef = false) {
            Name = name;
            Value = value;
            NoDef = nodef;
        }
    }

    public class Expression {
        public Expression Left;
        public Token Op;
        public Expression Right;

        public object Literal;

        public bool Print = false;
        public bool Group = false;
        
        public bool IsArray = false;
        public bool Linked = false;
        public Expression LastArrayExpr;
        public ArrayCollection LastArray;
        public List<Expression> Array;

        public bool Call = false;
        public List<Expression> Arguments;
        public Expression EarlyCall;
        public List<Expression> LateCall;
        public string Func;

        public Parser Parser;

        public bool ResolvedAsArray = false;
        public object ResolvedAs;

        // Just a temporary holder
        public object Temp;

        public object Accept(Interpreter intrp, bool lateignore = false) {
            
            if (Print) {
                intrp.Print(Right.Accept(intrp));
                return null;
            }

            if (Call) {
                ResolvedAs = intrp.Call(Func, Arguments);
                return ResolvedAs;
            }

            if (LateCall != null) {

                if (EarlyCall.LateCall != null) {
                    
                    intrp.LastRet = EarlyCall.Accept(intrp);
                    
                    ResolvedAs = intrp.LateCall(LateCall);
                    return ResolvedAs;
                }

                EarlyCall.Accept(intrp);
                ResolvedAs = intrp.LateCall(LateCall);
                return ResolvedAs;
            }

            if (Group) {
                ResolvedAs = ((Expression) Literal).Accept(intrp);

                return ResolvedAs;
            }

            if (IsArray) {
                if (LastArrayExpr != null && LastArray == null) {
                    LastArray = LastArrayExpr.Accept(intrp) as ArrayCollection;
                }

                /*if (LastArray != null && Temp != null) {
                    string ident = (Temp as Token).Lexeme;
                    ResolvedAs = intrp.GetVar(ident);

                    Console.WriteLine((ResolvedAs as ArrayCollection).Values[0]);
                        
                    return ResolvedAs;
                }*/

                if (LastArray != null) {
                    ResolvedAs = intrp.Index(LastArray, Array[0].Accept(intrp));

                    return ResolvedAs;
                }
                
                ResolvedAs = intrp.Array(Array);
                return ResolvedAs;
            }

            if (Literal is Token) {
                if (((Token) Literal).Type == TType.IDENT) {

                    object val = intrp.GetVar(((Token) Literal).Lexeme);
                    
                    if (val is ArrayCollection && Parser.PeekExpr().IsArray) {
                        ArrayCollection temp = val as ArrayCollection;
                        object value = null;

                        while (Parser.PeekExpr() != null && (Parser.PeekExpr().IsArray || value == null)) {
                            try {
                                ArrayCollection index = Parser.PeekExpr().Accept(intrp) as ArrayCollection;
                                List<Expression> indexexpr = Parser.PeekExpr().Array;

                                value = intrp.Index(temp, index.Values[0]);

                                Expression ex = Parser.PeekExpr();

                                if (ex == null) {
                                    break;
                                }

                                ex.Array = indexexpr;
                                ex.LastArray = temp;
                                
                                temp = ex.Accept(intrp) as ArrayCollection;
                                
                                Parser.Assign();
                            }
                            catch (Exception e) {
                                Console.WriteLine(e.Message);
                                break;
                            }
                        }
                        
                        return value == null ? intrp.Index(val as ArrayCollection, temp.Values[0]) : value;
                    }

                    ResolvedAs = val != null ? val : ((Token) Literal).Lexeme;

                    return ResolvedAs;
                }
            }

            if (Op != null) {
                ResolvedAs = intrp.Binary(Left.Accept(intrp), Op, Right.Accept(intrp));

                return ResolvedAs;
            }

            if (Literal != null)
                ResolvedAs = Literal;

            return ResolvedAs;
        }

        public Expression(Parser parser) {
            Parser = parser;
        }

        public Expression(Parser parser, object literal) {
            Parser = parser;
            Literal = literal;
        }
    }

    public class Parser {
       
        private List<Token> Tokens = new List<Token>();
        public int Current = 0;

        private Token Peek() {
            return Tokens[Current];
        }

        private Token Prev() {
            return Tokens[Current - 1];
        }

        private Token Advance() {
            return Tokens[Current++];
        }

        public bool match(TType type) {
            if (Peek().Type == type) {
                Advance();
                return true;
            }

            return false;
        }

        private bool MatchSym(string sym) {
            if (Peek().Type == TType.SYMBOL && Peek().Lexeme == sym) {
                Advance();
                return true;
            }

            return false;
        }

        private bool MatchKey(string key) {
            if (Peek().Type == TType.KEYWORD && Peek().Lexeme == key) {
                Advance();
                return true;
            }

            return false;
        }

        private Token Consume(TType type, string error) {
            if (match(type)) return Prev();

            throw new ParsingError(Peek(), error);
        }

        private Token ConsumeSym(string sym, string error) {
            if (MatchSym(sym)) return Prev();

            throw new ParsingError(Peek(), error);
        }

        /*if (match(TType.IDENT)) {
                Token ident = Prev();

                if (match(TType.LPAREN)) {
                    Advance();
                }

                if (MatchSym("=")) {
                    return new Statement(ident.Lexeme, Assign());
                }
            }*/

        public List<Statement> Found = new List<Statement>();
        public int CurStatement = 0;

        private Statement CallChain(Expression bas) {
            Expression newexpr = new Expression(this);
            
            while (true) {
                if (Peek().Type != TType.LPAREN) break;
                Expression temp = newexpr;

                newexpr = new Expression(this);
                newexpr.EarlyCall = temp.EarlyCall != null ? temp : bas;
                newexpr.LateCall = Parameters();
            }
            
            return new Statement(newexpr);
        }

        public Statement PeekStmt() {
            int cur = Current;
            Statement stmt = Declaration();
            Current = cur;
            return stmt;
        }

        public Expression PeekExpr() {
            int cur = Current;
            Expression expr = Assign();
            Current = cur;
            return expr;
        }

        public Statement Declaration() {
            if (match(TType.EOF)) {
                Statement stmt = new Statement();
                stmt.End = true;

                Found.Add(stmt);
                CurStatement++;

                return stmt;
            }
            
            if (match(TType.KEYWORD)) {
                Token keyword = Prev();

                if (keyword.Lexeme == "let") {

                    string name = Advance().Lexeme;
                    if (! MatchSym("=")) throw new Exception("Expected '='");

                    Statement stmt = new Statement(name, Assign());

                    Found.Add(stmt);
                    CurStatement++;

                    return stmt;
                }

                if (keyword.Lexeme == "print") {
                    Expression expr = new Expression(this);
                    expr.Right = Assign();
                    expr.Print = true;

                    Statement stmt = new Statement(expr);

                    Found.Add(stmt);
                    CurStatement++;

                    return stmt;
                }

                if (keyword.Lexeme == "fn") {
                    Statement stmt = new Statement();
                    
                    string name = Consume(TType.IDENT, "Expected identifier after 'fn'").Lexeme;
                    List<string> args = ParametersDef();

                    stmt.Name = name;
                    stmt.FuncDef = true;
                    stmt.Arguments = args;
                    stmt.Block = Block();

                    Found.Add(stmt);
                    CurStatement++;

                    return stmt;
                }

                if (keyword.Lexeme == "if") {
                    Expression condition = Assign();

                    Statement result = new Statement();

                    result.If = true;
                    result.Condition = condition;
                    result.IfBlock = new Statement(Block());

                    Found.Add(result);
                    CurStatement++;

                    return result;
                }

                if (keyword.Lexeme == "return") {
                    Statement stmt = new Statement();
                    stmt.Return = Assign();

                    Found.Add(stmt);
                    CurStatement++;

                    return stmt;
                }
            }

            if (match(TType.IDENT)) {
                Token ident = Prev();

                if (MatchSym("=")) {
                    Statement stmt =  new Statement(ident.Lexeme, Assign(), true);

                    Found.Add(stmt);
                    CurStatement++;

                    return stmt;
                }

                if (match(TType.LPAREN)) {
                    Current--;
                    List<Expression> args = Parameters();

                    Expression expr = new Expression(this);
                    expr.Call = true;
                    expr.Func = ident.Lexeme;
                    expr.Arguments = args;

                    if (Peek().Type == TType.LPAREN) {
                        Statement stmt = CallChain(expr);

                        Found.Add(stmt);
                        CurStatement++;

                        return stmt;
                    }

                    Found.Add(new Statement(expr));
                    CurStatement++;

                    return new Statement(expr);
                }
            }

            if (match(TType.LSQUARE)) {
                    Current--;
                    return new Statement(Array());
                }

            return new Statement(Assign());
        }

        public Expression Assign() {
            Expression left = Bitwise();

            if (MatchSym("=")) {
                Token op = Prev();
                Expression right = Assign();

                Expression result = new Expression(this);
                result.Left = left;
                result.Op = op;
                result.Right = right;
                return result;
            }

            return left;
        }

        public Expression Bitwise() {
            Expression left = Logical();

            if (MatchSym("|")) {
                Token op = Prev();
                Expression right = Logical();

                Expression result = new Expression(this);
                result.Left = left;
                result.Op = op;
                result.Right = right;
                return result;
            }

            return left;
        }

        public Expression Logical() {
            Expression left = Equality();

            if (MatchSym("||") || MatchSym("&&")) {
                Token op = Prev();
                Expression right = Equality();
                
                Expression result = new Expression(this);
                result.Left = left;
                result.Op = op;
                result.Right = right;
                return result;
            }

            return left;
        }

        public Expression Equality() {
            Expression left = Comparison();
            
            if (MatchSym("==")) {
                Token op = Prev();
                Expression right = Comparison();

                Expression result = new Expression(this);
                result.Left = left;
                result.Op = op;
                result.Right = right;
                return result;
            }

            return left;
        }

        public Expression Comparison() {
            Expression left = AddSub();
            
            if (MatchSym(">") || MatchSym("<")) {
                Token op = Prev();
                Expression right = AddSub();
                
                Expression result = new Expression(this);
                result.Left = left;
                result.Op = op;
                result.Right = right;
                return result;
            }

            return left;
        }

        public Expression AddSub() {
            Expression left = MulDiv();
            
            if (MatchSym("+") || MatchSym("-")) {
                Token op = Prev();
                Expression right = MulDiv();
                
                Expression result = new Expression(this);
                result.Left = left;
                result.Op = op;
                result.Right = right;
                return result;
            }

            return left;
        }

        public Expression MulDiv() {
            Expression left = Unary();
            
            if (MatchSym("*") || MatchSym("/")) {
                Token op = Prev();
                Expression right = Unary();
                
                Expression result = new Expression(this);
                result.Left = left;
                result.Op = op;
                result.Right = right;
                return result;
            }

            return left;
        }

        public Expression Unary() {

            return Primary();
        }

        public Expression Index() {
            Expression newexpr = new Expression(this);

            // So we don't reference start more than once
            Expression start = newexpr;
            
            while (true) {
                if (Peek().Type != TType.LSQUARE) break;
                Expression temp = newexpr;

                newexpr = Array();
                
                if (temp != start)
                    newexpr.LastArrayExpr = temp;
            }
            
            
            return newexpr;
        }

        public Expression Primary() {

            if (match(TType.IDENT)) {
                Token ident = Prev();

                if (match(TType.LPAREN)) {
                    Current--;
                    List<Expression> args = Parameters();

                    Expression expr = new Expression(this);
                    expr.Call = true;
                    expr.Func = ident.Lexeme;
                    expr.Arguments = args;

                    if (Peek().Type == TType.LPAREN) {
                        return CallChain(expr).Expr;
                    }

                    return expr;
                }

                /*if (match(TType.LSQUARE)) {
                    Current--;
                    Expression expr = Index();

                    expr.Temp = ident;

                    return expr;
                }*/
                
                return new Expression(this, ident);
            }
            
            if (match(TType.LPAREN)) {
                Expression expr = new Expression(this);
                //int cur = Current;
                //Current--;
                //expr.LateCall = Parameters();
                //Current = cur;
                expr.Literal = Assign();
                expr.Group = true;
                Advance();

                return expr;
            }

            if (match(TType.LSQUARE)) {
                Current--;

                return Array();
            }

            if (match(TType.NUMBER)) {
                Token num = Prev();
                
                return new Expression(this, double.Parse(num.Lexeme)); // 5 == 5 == 2 == 1
            }

            if (match (TType.STRING)) {
                return new Expression(this, Prev().Lexeme);
            }

            return null;
        }

        public List<Expression> Parameters() {
            List<Expression> exprs = new List<Expression>();

            Token prev = Prev();

            Consume(TType.LPAREN, $"Expected '(' after '{prev.Lexeme}'");

            // Does checking inside the loop
            while (! match(TType.RPAREN)) {
                exprs.Add(Assign());

                if (match(TType.RPAREN)) break;

                prev = Prev();

                ConsumeSym(",", $"Expected comma after expression '{prev.Lexeme}' got '{Peek().Lexeme}'");
            }

            return exprs;
        }

        public Expression Array() {
            List<Expression> exprs = new List<Expression>();

            Token prev = Prev();

            Consume(TType.LSQUARE, $"Expected '[' after '{prev.Lexeme}'");

            // Does checking inside the loop
            while (! match(TType.RSQUARE)) {
                exprs.Add(Assign());

                if (match(TType.RSQUARE)) break;

                prev = Prev();

                ConsumeSym(",", $"Expected comma after expression '{prev.Lexeme}' got '{Peek().Lexeme}'");
            }

            Expression list = new Expression(this);
            list.IsArray = true;
            list.Array = exprs;

            return list;
        }

        public List<string> ParametersDef() {
            List<string> idents = new List<string>();

            Token prev = Prev();

            Consume(TType.LPAREN, $"Expected '(' after '{prev.Lexeme}'");

            // Does checking inside the loop
            while (! match(TType.RPAREN)) {
                idents.Add(Consume(TType.IDENT, $"Expected identifier after '{Prev().Lexeme}'").Lexeme);

                if (match(TType.RPAREN)) break;

                prev = Prev();

                ConsumeSym(",", $"Expected comma after expression '{prev.Lexeme}'");
            }

            return idents;
        }

        public List<Statement> Block() {
            if (! match(TType.LBRACE))
                throw new ParsingError(Peek(), $"Expected '{{' to start block near '{Peek().Lexeme}");

            List<Statement> block = new List<Statement>();

            while (! match(TType.RBRACE)) {

                if (Peek().Type == TType.EOF)
                    throw new ParsingError(Prev(), $"Expected '}' at '{Prev().Lexeme}'");

                block.Add(Declaration());
            }

            return block;
        }
        
        public Parser(string code) {
            Tokens = new Ghost.Lexer.Lexer(code).GetTokens();
        }
    }
}