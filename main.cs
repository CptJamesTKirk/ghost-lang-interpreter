using System;
using System.IO;
using Ghost;
using Ghost.Parser;

class MainClass {
  public static void Main (string[] args) {
    Interpreter intrp = new Interpreter();

    Parser parse = new Parser(new StreamReader("./code.gh").ReadToEnd());

    while (true) {
        Statement stmt = parse.Declaration();

        if (stmt.End) return;

        stmt.Accept(intrp);
    }
  }
}